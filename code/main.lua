-- from https://fennel-lang.org/tutorial

local fennel = require("fennel")
local f = io.open("game.fnl", "rb")
local game = fennel.dofile("game.fnl")
f:close()

love.load = function()
   game.load()
end

love.update = function(dt)
   game.update(dt)
end

love.draw = function()
   game.draw()
end

love.mousepressed = function(x, y, button, isTouch, presses)
   game.mousepressed(x, y, button)
end

love.keypressed = function(key)
   if(key == "f5") then -- support reloading
      for k,v in pairs(fennel.dofile("game.fnl")) do
         game[k] = v
      end
   end
end
