You have to have Lua and Fennel installed to

Things you need to have installed

- [Lua](https://www.lua.org/)
- (optionally) [Luarocks](https://luarocks.org/) (to install Fennel)
- [Fennel](https://fennel-lang.org)
- [Love2d](https://love2d.org/)

Then run `$ love .`

![screen cap gif](screencap.gif)

### Image credits

- [Foeniculum_vulgare_knolle](https://commons.wikimedia.org/wiki/Foeniculum_vulgare?uselang=de#/media/File:Foeniculum_vulgare_knolle.jpeg), by Kristian Peters, [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- [Five_spices_detailed](https://en.wikipedia.org/wiki/Five-spice_powder#/media/File:Five_spices_detailed.jpg), by [Tim Sackton](https://www.flickr.com/photos/sackton/), [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)
- [Orlen_Warsaw_Marathon_2014_al](https://en.wikipedia.org/wiki/Marathon#/media/File:Orlen_Warsaw_Marathon_2014_al._KEN.JPG), by [Adrian Grycuk](https://commons.wikimedia.org/wiki/User:Boston9), [CC BY-SA 3.0 pl](https://creativecommons.org/licenses/by-sa/3.0/pl/deed.enhttps://creativecommons.org/licenses/by-sa/3.0/pl/deed.en)
- [Heinrich_fueger_1817_prometheus_brings_fire_to_mankind](https://en.wikipedia.org/wiki/Prometheus#/media/File:Heinrich_fueger_1817_prometheus_brings_fire_to_mankind.jpg), by Heinrich Füger, Public Domain
- [The Drinkers](https://en.wikipedia.org/wiki/Absinthe#/media/File:Jean_B%C3%A9raud_The_Drinkers.jpg), by Jean Béraud, Public Domain
