love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "Four Fun Fennel Facts", "Four Fun Fennel Facts"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 500
   t.window.height = 800
   t.window.vsync = false
   t.version = "11.2"
end
